Pepper&Carrot: The SLiM theme
=============================

![Screenshot of the theme preview](screenshot.jpeg)

This is a Pepper&Carrot theme for the login manager SLiM.
It has several variants, depending on your screen size: 16:10 and 16:9.
The background is taken from
[episode 9](https://www.peppercarrot.com/en/article289/episode-9-the-remedy)
of a beautiful comics [Pepper&Carrot](https://www.peppercarrot.com/)
created by [David Revoy](https://www.davidrevoy.com/)
and cropped to fit the resolutions (1920×*).

Installation
------------
Simply choose one folder that fits your screen and move it to
`/usr/share/slim/themes/` (you can also rename it to `pepperandcarrot` if you
like). Then set the theme in `/etc/slim.conf` (property `current_theme` – the
value is the same as the folder name).

Licence
-------
The artwork was created by [David Revoy](https://www.davidrevoy.com/) and is
licenced under Creative Commons Attribution 4.0.

The panel is licenced under GPLv2 as it is borrowed from the default SLiM theme.
The theme file is also licenced under GPLv2 as it is based on the default SLiM
theme.
